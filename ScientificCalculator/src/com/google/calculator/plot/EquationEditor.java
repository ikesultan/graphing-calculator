// Copyright 2008 Google Inc.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.google.calculator.plot;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.inputmethodservice.KeyboardView.OnKeyboardActionListener;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.calculator.HelpFragment;
import com.google.calculator.R;
import com.google.calculator.ScientificCalculator;
import com.google.calculator.ScientificCalculatorApplication;
import com.google.calculator.calc.InputTextView;

/**
 * Activity Equation Editor.
 * 
 * @author tulaurent@gmail.com (Laurent Tu)
 */
public class EquationEditor extends Activity {
  private static final int ADD_EQUATION_ID = 0;
  private static final int PLOT_ID = 1;
  private static final int HELP_ID = 2;
  private EquationEditorView equationsView;
  private String current;
  private int currentEq;


  @Override
  public void onCreate(Bundle icicle) {
    super.onCreate(icicle);
    setContentView(R.layout.equation_editor);
    equationsView = (EquationEditorView) findViewById(R.id.equations);
    
    current = "";
    Keyboard mKeyboard= new Keyboard(this,R.xml.board);
    // Lookup the KeyboardView
    KeyboardView mKeyboardView= (KeyboardView)findViewById(R.id.keyboardview);
    // Attach the keyboard to the view
    mKeyboardView.setKeyboard( mKeyboard );
    // Do not show the preview balloons
    mKeyboardView.setPreviewEnabled(true);
    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    mKeyboardView.setOnKeyboardActionListener(new OnKeyboardActionListener() {

		@Override
		public void onKey(int keycode, int[] arg1) {
			EquationAdapter a = (EquationAdapter) equationsView.getAdapter();
			current = a.getEquationView(currentEq).getEditText().getText().toString();
			switch (keycode){
			case 55000:current=current+"sin";break;
			case 55001:current=current+"cos";break;
			case 55002:current=current+"tan";break;
			case 55003:current=current+"ln";break;
			case 55006:current = "";break;
			case 55008:current = current.length()>0 ? current.substring(0,current.length()-1) : "";break;
			//current = ""; break;
			case 55010:current = current+"x";break;
			case 55005:currentEq = (currentEq+1) % a.getSize();
			current = a.getEquationView(currentEq).getEditText().getText().toString();break;
			case 55007:currentEq = (((currentEq-1) % a.getSize())+a.getSize()) % a.getSize();
			current = a.getEquationView(currentEq).getEditText().getText().toString();break;
			default:current = current + Character.toString((char) keycode);break;
			}
			a.getEquationView(currentEq).getEditText().setText(current, InputTextView.BufferType.EDITABLE);
		}


		@Override
		public void onPress(int arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onRelease(int arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onText(CharSequence arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void swipeDown() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void swipeLeft() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void swipeRight() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void swipeUp() {
			// TODO Auto-generated method stub
			
		}


    	
    });
  }
  
  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.equationmenu, menu);
    menu.add(0, PLOT_ID, 0, R.string.menu_eq_editor_plot);
    menu.add(0, ADD_EQUATION_ID, 0, R.string.menu_eq_editor_add_equation);
    menu.add(0, HELP_ID, 0, R.string.menu_help);
    return true;
  } 
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
    // action with ID action_settings was selected
    case R.id.action_equation_editor:
        Intent i = new Intent(this, EquationEditor.class);
        i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivityForResult(i, 0);
        break;
    case R.id.action_calc:
    	i = new Intent(this, ScientificCalculator.class);
        i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
    	startActivityForResult(i, 0); 
        break;
    case R.id.action_graph:
    case PLOT_ID:
        ArrayList<PlotData> listPlotData = equationsView.getPlotData();
        ScientificCalculatorApplication application = (ScientificCalculatorApplication) getApplication();
        application.setLastPlotData(listPlotData);
        i = new Intent(this, EquationPlotter.class);
        i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivityForResult(i, 0);
        break;
    case R.id.action_add_equation:  
    case ADD_EQUATION_ID:
        equationsView.addEquation();
        break;
    case HELP_ID:
		(new HelpFragment()).show(this.getFragmentManager(), ""); break;

    default:
      break;
    }

    return true;
  } 
  
}