// Copyright 2008 Google Inc.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.google.calculator.plot;

import com.google.calculator.HelpFragment;
import com.google.calculator.R;
import com.google.calculator.ScientificCalculator;
import com.google.calculator.ScientificCalculatorApplication;
import com.google.calculator.SettingsFragment;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

/**
 * Activity plotting the equation.
 * 
 * @author tulaurent@gmail.com (Laurent Tu)
 *
 */
public class EquationPlotter extends Activity {

  private EquationPlotterView plotterView;
  private static final int EQ_EDITOR_ID = 0;
  private static final int PLOT_ID = 1;
  private static final int HELP_ID = 2;
  
  @Override
  public void onCreate(Bundle icicle) {
    super.onCreate(icicle);
    setContentView(R.layout.equation_plotter);
    plotterView = (EquationPlotterView) findViewById(R.id.plotter);
    
    ScientificCalculatorApplication application = (ScientificCalculatorApplication) getApplication();
    ArrayList<PlotData> lastPlotData = application.getLastPlotData();
    plotterView.setListPlotData(lastPlotData);
  }
  
  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.graphmenu, menu);
    menu.add(0, PLOT_ID, 0, R.string.menu_eq_editor_plot);
    menu.add(0, HELP_ID, 0, R.string.menu_help);
    return true;
  } 
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
    case R.id.action_equation_editor:
        Intent i = new Intent(this, EquationEditor.class);
        i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivityForResult(i, 0);
        break;
    case R.id.action_calc:
        i = new Intent(this, ScientificCalculator.class);
        i.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivityForResult(i, 0);
        break;
	case HELP_ID:
		(new HelpFragment()).show(this.getFragmentManager(), ""); break;
    default:
      break;
    }
    return true;
  }
}
