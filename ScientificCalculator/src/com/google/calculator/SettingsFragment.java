package com.google.calculator;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class SettingsFragment extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
       /* builder.setMultiChoiceItems(R.array.settings_array, null,
                new DialogInterface.OnMultiChoiceClickListener() {
         @Override
         public void onClick(DialogInterface dialog, int which,
                 boolean isChecked) {
             if (isChecked) {
                 // If the user checked the item, add it to the selected items
                 
             } 
         }
     })*/
       builder.setMessage("Do you want to be in degrees or radians?")
               
        .setPositiveButton(R.string.degrees, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       ScientificCalculator.inDegrees =true;
                   }
               })
               .setNegativeButton(R.string.radians, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   ScientificCalculator.inDegrees =false;
                   }
               });
               
        // Create the AlertDialog object and return it
        return builder.create();
    }
}