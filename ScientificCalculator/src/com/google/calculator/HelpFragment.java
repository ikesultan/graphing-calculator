package com.google.calculator;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.widget.TextView;

public class HelpFragment extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	final TextView message = new TextView(this.getActivity());
    	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    	String str = getString(R.string.help);
    	final SpannableString s = 
                new SpannableString(str);
    	Linkify.addLinks(s, Linkify.WEB_URLS);
    	message.setText(s);
    	message.setMovementMethod(LinkMovementMethod.getInstance());
    	
    	
    	return builder.setView(message).create();
    }
}