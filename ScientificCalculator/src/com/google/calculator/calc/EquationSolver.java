package com.google.calculator.calc;

import com.google.calculator.tree.EvaluationContext;
import com.google.calculator.tree.Tree;



public class EquationSolver {


	
	public static double eval(String str) {
		Tree tree;
		try {
		      tree = Computer.parseLine(str);
		      double d = tree.evaluate(EvaluationContext.getDefaultContext());
		      return d;
		    } catch (Exception e) {
		    }
		return 0;
	}
	public static double bisection(String str){
		double left = -2000000000;
		double right = 2000000000;
		int counter = 0;
		double mid = 0; 
		while (counter<100){
			mid = (right - left) /2;
			
		}
		return mid;
	}
	public static double newton(String str) throws Exception {
		double x = 1; //bisection(str);
		int counter = 0;
		double fx0=eval(toSolve(str, x));
		if (!str.contains("x")) {
			throw new Exception("equation invalid.");
		}
		while (Math.abs(fx0)>0.0001 && counter<10000) {
			double dx = derivative(str, x);
			if(Math.abs(dx)>0.01) {
				x = x - (fx0/dx);
				fx0 = eval(toSolve(str, x));
				counter++;
			} else {x += 7;}
		} 
		x = (double)Math.round(x * 10000) / 10000;
		return x;
	}
	
	
	
	public static double derivative(String str, double a) {
		double b = a + 0.000001;
		String one = toSolve(str, a);
		String two  = toSolve(str,b);
		return (eval(two) - eval(one)) / 0.000001;
	}
	
	
	
	public static String toSolve(String str, double a) {
		String result = "";
		String ab = a + "";
		for (int x = 0; x < str.length(); x++) {
			if (x>0 && Character.isDigit(str.charAt(x-1)) && str.charAt(x)=='x') {
				result += "*" + a;
			} 
			else if (str.substring(x,x+1).equals("x")) {
				result += a;
			}
			else {
				result = result + str.substring(x,x+1);
			}
		}
		return result;
	}
	
	public static String degToRad(String str) {
		String result = "";
		for (int x = 0; x < str.length() - 2; x++) {
			String sub = str.substring(x, x+3);
			if (sub.equals("sin") || sub.equals("cos") || sub.equals("tan")) {
				int paren = 1;
				int index;
				for (index = x + 4; index < str.length() && paren > 0; index++) {
					if (str.charAt(index) == '(') {
						paren++;
					}
					else if (str.charAt(index) == ')') {
						paren --;
					}
				}
				String exp = str.substring(x+4, index-1);
				double y = eval(exp);
				double z = (Math.PI*y)/180;
				result = str.substring(0,x+4)+ z + str.substring(index-1);
			}
		}
		return result;
	}
	


}